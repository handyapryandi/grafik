import matplotlib.pyplot as plt
import numpy as np


#Declare Font
font = {'family': 'Arial',
        'color':  'Black',
        'weight': 'medium',
        'size': 18,
        }

#Declare the data
data = {'Device': [15.543, 15.538, 15.534, 15.531, 15.528, 15.526, 15.524, 15.523, 15.522, 15.520, 15.519, 15.519,
                   15.518, 15.517, 15.517, 15.516, 15.516, 15.514, 15.514, 15.514, 15.514, 15.514, 15.514, 15.513,
                   15.513, 15.513, 15.513, 15.512, 15.512, 15.512, 15.512, 15.512, 15.511, 15.511, 15.511, 15.511,
                   15.510],

        'Scope': [15.860, 15.850, 15.850, 15.840, 15.850, 15.850, 15.860, 15.860, 15.860, 15.860, 15.860, 15.850,
                  15.850, 15.850, 15.850, 15.850, 15.850, 15.850, 15.830, 15.830, 15.830, 15.830, 15.830, 15.820,
                  15.820, 15.820, 15.820, 15.820, 15.820, 15.820, 15.820, 15.820, 15.810, 15.810, 15.810, 15.810,
                  15.810],

        'time': ['11:30:00', '11:35:00', '11:40:00', '11:45:00', '11:50:00', '11:55:00', '12:00:00', '12:05:00',
                 '12:10:00', '12:15:00', '12:20:00', '12:25:00', '12:30:00', '12:35:00', '12:40:00', '12:45:00',
                 '12:50:00', '12:55:00', '13:00:00', '13:05:00', '13:10:00', '13:15:00', '13:20:00', '13:25:00',
                 '13:30:00', '13:35:00', '13:40:00', '13:45:00', '13:50:00', '13:55:00', '14:00:00', '14:05:00',
                 '14:10:00', '14:15:00', '14:20:00', '14:25:00', '14:30:00'
                    ]}

#Size the plot
fig = plt.figure(figsize=(15, 8))


#Make the plot
y1 = plt.plot(data['Device'], label='Device') #label the line1 as Device
y2 = plt.plot(data['Scope'], label='Scope') #label the line2 as Device
plt.xticks(range(len(data['time'])), data['time'])
plt.yticks(np.arange(15.3, 16.1, 0.1)) #arange the scale between 15,3 and 16,1

#plt.setp(y1, linestyle='--')       # set to dashed
plt.setp(y1, linewidth=3, color='r', marker='o')  # line1 is thick and red
plt.setp(y2, linewidth=3, color='g', marker='s')  # line2 is thick and green


#Show the Grid With the Line Style
plt.grid(True, linestyle='--')

#Label the title, xasis, yaxis
plt.title('Measurement of Device And Scope', size=20)
plt.xlabel('Time', fontdict=font)
plt.ylabel('Voltage(Volt)', fontdict=font)

#Make xtick font size and rotate
plt.xticks(weight='bold', rotation=45)
plt.yticks(weight='bold', rotation=45)
    #weight='bold'
    #rotation=45
    #fontsize=14


#ax.tick_params(labelcolor='r', labelsize='medium', width=3)

# Draw a legend bar
plt.legend(loc='lower right')


#Make Graph Scretch in Windows
fig.tight_layout()

#show
plt.show()