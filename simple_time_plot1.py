import matplotlib.pyplot as plt
import numpy as np

#Declare the data
data = {'Device': [15.530, 15.525, 15.520, 15.517,	15.515,	15.514,	15.513,	15.512,	15.512,	15.511,	15.511,
                     15.512,	15.510,	15.510,	15.510],

        'Scope': [15.860, 15.840, 15.840, 15.840, 15.850, 15.830, 15.840, 15.840, 15.830, 15.840, 15.830, 15.830,
                  15.830, 15.840, 15.830],

        'time': ['09:40:00', '09:50:00', '10:00:00', '10:10:00', '10:20:00', '10:30:00', '10:40:00', '10:50:00',
                 '11:00:00', '11:10:00', '11:20:00', '11:30:00', '11:40:00', '11:50:00', '12:00:00']}

#Size the plot
fig = plt.figure(figsize=(15, 8))

#Make the plot
plt.plot(data['Device'], label='Device')
plt.plot(data['Scope'], label='Scope')
plt.xticks(range(len(data['time'])), data['time'])
plt.yticks(np.arange(15.3, 16.1, 0.1))

#Show the Grid
plt.grid(True)

#Label the title, xasis, yaxis
plt.title('Measurement of Device And Scope')
plt.xlabel('Time')
plt.ylabel('Voltage(Volt)')

# Draw a legend bar
plt.legend(loc='lower right')

#Make Graph Scretch in Windows
fig.tight_layout()

plt.show()