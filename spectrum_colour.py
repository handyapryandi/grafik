import numpy as np
import matplotlib.pyplot as plt


#Read Data From File
x, y = np.loadtxt('spectrum.txt', unpack=True)

#Make the plot
plt.plot(x, y, color='darkblue', marker='o', linewidth=2)

#add title and label
plt.title('Spectrum of Colour', size=20)
plt.xlabel('Wavelength [nm]', size=16)
plt.ylabel('Radiance [W/(sr*sqm*nm)]', size=16)

#add grid
plt.grid(True, linestyle='-.')

#Make xtick font size and rotate
plt.xticks(weight='semibold')
plt.yticks(weight='semibold')

#show
plt.show()